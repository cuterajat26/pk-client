import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import pkg from './package.json';

export default [
    // browser-friendly UMD build
    {
        input: './index.js',
        output: {
            preferBuiltins: false,
            name: 'PKClient',
            file: pkg.browser,
            format: 'umd'
        },
        plugins: [
            resolve(), // so Rollup can find `ms`
            commonjs() // so Rollup can convert `ms` to an ES module
        ]
    },
    {
        input: './index.js',
        external: ['crypto-browserify', 'buffer'],
        output: [{
                preferBuiltins: false,
                file: pkg.main,
                format: 'cjs'
            },
            {
                preferBuiltins: false,
                file: pkg.module,
                format: 'es'
            }
        ]
    }

];