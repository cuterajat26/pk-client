import { before, after } from "intern/lib/interfaces/bdd";
import { ModelFactory, MODEL_PATHS, SessionChallenge } from '../index';

const { describe, it } = intern.getInterface('bdd');
const { assert } = intern.getPlugin('chai');

describe('SessionChallengeTest', () => {

    let base_url: string;
    let username: string;
    let password: string;
    let token: string;
    before(() => {
        base_url = process.env.TEST_BASE_URL;
        username = process.env.TEST_USERNAME;
        password = process.env.TEST_PASSWORD;
        assert.ok(base_url, "Please set environment variable `TEST_BASE_URL`");
        assert.ok(username, "Please set environment variable `TEST_USERNAME`");
        assert.ok(password, "Please set environment variable `TEST_PASSWORD`");
        return;
    });


    it(`test login`, (done) => {

        return (async () => {
            let result = await (new SessionChallenge(base_url,username,password)).login();
            assert.ok(result.token);
            token = result.token;
        })();
    });


    it(`test session token by getting logged in user details`, (done) => {
        return (async () => {
            let mf = new ModelFactory({ api_base_url: base_url, token });
            let emp = await mf.anyModel(MODEL_PATHS.employees).get("me");
            assert.ok(emp);
        })();

    });

    after(() => {
        return (async () => {

        })();

    })
});

