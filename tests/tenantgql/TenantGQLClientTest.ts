import { before, after } from "intern/lib/interfaces/bdd";
import { ModelFactory, MODEL_PATHS, GQL_PATHS, SessionChallenge } from '../../index';

const { describe, it } = intern.getInterface('bdd');
const { assert } = intern.getPlugin('chai');

describe('TenantGQLClient Test', () => {


    let token: string;
    let base_url: string;
    let username: string;
    let password: string;
    before(async () => {
        base_url = process.env.TEST_BASE_URL;
        token = process.env.TEST_TOKEN;
        assert.ok(base_url, "Please set environment variable `TEST_BASE_URL`");
        assert.ok(token, "Please set environment variable `TEST_TOKEN`");
        // assert.ok(username, "Please set environment variable `TEST_USERNAME`");
        // assert.ok(password, "Please set environment variable `TEST_PASSWORD`");
        
        return;
    });

    it(`test tenant_gql_client `, (done) => {

        return (async () => {
            let variables = { "entityId": "rajat-talwar",
            }
             token = "xxxx.xxxx";
                base_url = "https://xxxxx";
            let mf = new ModelFactory({ api_base_url: base_url, token });
            let response = await mf.tenant_gql_client('','EQPAYSACK').mutation('set_encrypted_pin_data', {...variables}, 'status');
            console.log(response);
            assert.ok(response);

        })();

    });

    after(() => {
        return (async () => {

        })();

    })
});

