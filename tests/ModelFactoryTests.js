"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var bdd_1 = require("intern/lib/interfaces/bdd");
var index_1 = require("../index");
var _a = intern.getInterface('bdd'), describe = _a.describe, it = _a.it;
var assert = intern.getPlugin('chai').assert;
describe('ModelFactoryTests', function () {
    var token;
    var base_url;
    var username;
    var password;
    bdd_1.before(function () { return __awaiter(_this, void 0, void 0, function () {
        var result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    base_url = process.env.TEST_BASE_URL;
                    username = process.env.TEST_USERNAME;
                    password = process.env.TEST_PASSWORD;
                    assert.ok(base_url, "Please set environment variable `TEST_BASE_URL`");
                    assert.ok(username, "Please set environment variable `TEST_USERNAME`");
                    assert.ok(password, "Please set environment variable `TEST_PASSWORD`");
                    return [4 /*yield*/, (new index_1.SessionChallenge(base_url, username, password)).login()];
                case 1:
                    result = _a.sent();
                    assert.ok(result.token);
                    token = result.token;
                    return [2 /*return*/];
            }
        });
    }); });
    it("test employees model", function (done) {
        return (function () { return __awaiter(_this, void 0, void 0, function () {
            var mf, emp;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        mf = new index_1.ModelFactory({ api_base_url: base_url, token: token });
                        return [4 /*yield*/, mf.employees().get("me")];
                    case 1:
                        emp = _a.sent();
                        assert.ok(emp);
                        return [2 /*return*/];
                }
            });
        }); })();
    });
    it("test anyModel", function (done) {
        return (function () { return __awaiter(_this, void 0, void 0, function () {
            var mf, emp;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        mf = new index_1.ModelFactory({ api_base_url: base_url, token: token });
                        return [4 /*yield*/, mf.anyModel(index_1.MODEL_PATHS.employees).get("me")];
                    case 1:
                        emp = _a.sent();
                        assert.ok(emp);
                        return [2 /*return*/];
                }
            });
        }); })();
    });
    it("test gql", function (done) {
        return (function () { return __awaiter(_this, void 0, void 0, function () {
            var mf, query, gqlResult;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        mf = new index_1.ModelFactory({ api_base_url: base_url, token: token });
                        query = "query GetReports{\n                            reports{\n                                registeredEmployeeCount\n                            }\n                        }";
                        return [4 /*yield*/, mf.gql(index_1.GQL_PATHS.comp_reports).query('reports', {}, 'registeredEmployeeCount')];
                    case 1:
                        gqlResult = _a.sent();
                        assert.ok(gqlResult.data.reports.registeredEmployeeCount);
                        return [2 /*return*/];
                }
            });
        }); })();
    });
    bdd_1.after(function () {
        return (function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        }); })();
    });
});
