import { before, after } from 'intern/lib/interfaces/bdd';
import {
  ModelFactory,
  MODEL_PATHS,
  GQL_PATHS,
  SessionChallenge
} from '../index';

const { describe, it } = intern.getInterface('bdd');
const { assert } = intern.getPlugin('chai');

describe('WalletAPITest', () => {
  let token: string;
  let base_url: string;
  let username: string;
  let password: string;
  before(async () => {
    base_url = process.env.TEST_BASE_URL;
    username = process.env.TEST_USERNAME;
    password = process.env.TEST_PASSWORD;
    assert.ok(base_url, 'Please set environment variable `TEST_BASE_URL`');
    assert.ok(username, 'Please set environment variable `TEST_USERNAME`');
    assert.ok(password, 'Please set environment variable `TEST_PASSWORD`');
    let result = await new SessionChallenge(
      base_url,
      username,
      password
    ).login();
    assert.ok(result.token);
    token = result.token;
    return;
  });

  it(`test employee wallet`, done => {
    return (async () => {
      let mf = new ModelFactory({ api_base_url: base_url, token });
      let emp = await mf.employees().get('me');
      let wallet = await mf
        .anyModel(<any>MODEL_PATHS.employee_wallet.replace(':empId', emp._id))
        .get();
      assert.ok(wallet);
    })();
  });

  after(() => {
    return (async () => {})();
  });
});
