# Paysack/Huepay client module

A javascript(nodejs & browser) client library to talk to paysack and huepay apis.
See [Docs](https://docs.paysack.com)

## Install

```shell
npm install --save-dev pk-client
```


## Usage

### Authentication

```typescript
import { SessionChallenge } from "pk-client";
let sc = new SessionChallenge('<base_api_url>', 'xyz@gmail.com', 'password');


sc.login()
        .then(
            response => {
               console.log(response['token']);
            },
            err => {
                throw err;
            }
        );

```

### REST API requests


```typescript
import {ModelFactory,MODEL_PATHS} from "pk-client"; /*Typescript*/
// var  ModelFactory = require("pk-client").MODEL_PATHS;  /*Javascript*/
// var  MODEL_PATHS = require("pk-client").MODEL_PATHS; /*Javascript*/

let base_url = "https://<your-api-server></your-api-server>.com";
let mf = new ModelFactory({ api_base_url: base_url, token:"" });
try {
        let employee = await mf.anyModel(MODEL_PATHS.employees).get("me");
        let create_new = await mf.anyModel(MODEL_PATHS.employees).add({first_name:"abc",.....});
        let update_existing = await mf.anyModel(MODEL_PATHS.employees).put("5de6186d709db8cb2106812f",{first_name:"abc",.....});
    }
    catch (e) {
        console.log(e.toString());
        console.log(e.response.status);
    }
```


### GraphQL requests

```typescript

import {ModelFactory,GQL_PATHS} from "pk-client"; /*Typescript*/
// var  ModelFactory = require("pk-client").MODEL_PATHS;  /*Javascript*/
// var  GQL_PATHS = require("pk-client").GQL_PATHS; /*Javascript*/

let base_url = "https://<your-api-server></your-api-server>.com";
let token  = "<your_token>";

let mf = new ModelFactory({ api_base_url: base_url, token:"<your_token>" });
let gqlResult = await mf.gqlclient(`ms/graphql/comp-reports`).query('example_operation_1', {arg1:"value1",arg2:"value2"},'response_field_1 response_field_2');
let gqlResult = await mf.gqlclient(`ms/graphql/comp-reports`).mutation('example_operation_2', {arg1:"value1",arg2:"value2"},'response_field_1 response_field_2');
```



### Fetch Passbook

```typescript
let base_url = "https://<your-api-server></your-api-server>.com";
let token  = "<your_token>";

let mf = new ModelFactory({ api_base_url: base_url, token: token });

let timestamp_now = Date.now()/1000;
let timestamp_30_days_before = Date.now()/1000 - 30*24*60*60;

let passbook = await mf.passbookAdapter().fetch(  timestamp_now,
timestamp_30_days_before);
```


### Fetch Employee/Member Details
```typescript

let base_url = "https://<your-api-server></your-api-server>.com";
let token  = "<your_token>";

let mf = new ModelFactory({ api_base_url: base_url, token: token });

let employee = await mf.employees().get(`<_id>`);

let employee_all = await mf.employees().query({'pagination.limit':10,'pagination.field':'first_name','pagination.sort':'asc'}); //fetch all employees of a company

```

### Fetch Logged-in User Details

```typescript
const  MODEL_PATHS = require("pk-client").MODEL_PATHS;
let base_url = "https://<your-api-server></your-api-server>.com";
let token  = "<your_token>";

let mf = new ModelFactory({ api_base_url: base_url, token: token });

let user = await mf.anyModel(MODEL_PATHS.users).get('me');//fetches the logged in user


```

## Running Tests

```shell
TEST_BASE_URL="https://base.url.com/" TEST_USERNAME="abc@xyz.com" TEST_PASSWORD="xyzabcpqrs" npx intern

```

## Building 

> Make sure you have browserify and uglify-js installed

```shell
npm install -g uglify-js
npm install -g browseify

```

### Build client side libraries 

```shell
npm run dist
```




