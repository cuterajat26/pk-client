"use strict";
/**
 * Created by rohittalwar on 10/05/16.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var UserDefaults = /** @class */ (function () {
    function UserDefaults() {
    }
    UserDefaults.get = function (key) {
        return localStorage.getItem(key);
    };
    UserDefaults.put = function (key, obj) {
        localStorage.setItem(key, obj);
    };
    UserDefaults.remove = function (key) {
        localStorage.removeItem(key);
    };
    return UserDefaults;
}());
exports.default = UserDefaults;
