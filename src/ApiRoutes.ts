export enum MODEL_PATHS {
    employees = "/v1/employees",
    companies = "/v1/companies",
    card_kit = "/v1/card-kit",
    config_urls = "/v1/config/urls",
    employees_bulk = "/v1/employees/bulk",
    otp = "/v1/otp",
    otp_attach = "/v1/otp/attach",
    otp_login = "/v1/otp/login",
    signup = "/v1/signup",
    signup_email_verify = "/v1/signup/verify_email",
    email_verify = "/v1/authenticate/verify_email",
    users = "/v1/users",
    pending_users = "/v1/pending_users",
    company_transactions = "/v1/company_transactions",
    comp_emp_txns_bulk = "/v1/comp_emp_txns/bulk",
    comp_emp_txns_sync = "/v1/comp_emp_txns/sync",
    comp_emp_txns = "/v1/comp_emp_txns",
    merchant_transactions = "/v1/merchant_transactions",
    merchant_cash_transactions = "/v1/merchant_cash_transactions",
    AUTH2 = "/v2/session/challenge",
    forgot_password = "/v1/authenticate/forgotPassword",
    change_password = "/v1/authenticate/changePassword",
    employee_profile = "/v1/employees/profiles",
    employee_wallet = "/v1/employees/:empId/wallet",
}

export enum GQL_PATHS {
    emp_reports = "/graphql/empreports",
    comp_reports = "/graphql/comp-reports",
    login_service = "/graphql/login-service",
    org_admin_service = "/graphql/org-admin-service"
}