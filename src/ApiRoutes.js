"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MODEL_PATHS;
(function (MODEL_PATHS) {
    MODEL_PATHS["employees"] = "/v1/employees";
    MODEL_PATHS["companies"] = "/v1/companies";
    MODEL_PATHS["card_kit"] = "/v1/card-kit";
    MODEL_PATHS["config_urls"] = "/v1/config/urls";
    MODEL_PATHS["employees_bulk"] = "/v1/employees/bulk";
    MODEL_PATHS["otp"] = "/v1/otp";
    MODEL_PATHS["otp_attach"] = "/v1/otp/attach";
    MODEL_PATHS["otp_login"] = "/v1/otp/login";
    MODEL_PATHS["signup"] = "/v1/signup";
    MODEL_PATHS["signup_email_verify"] = "/v1/signup/verify_email";
    MODEL_PATHS["email_verify"] = "/v1/authenticate/verify_email";
    MODEL_PATHS["users"] = "/v1/users";
    MODEL_PATHS["pending_users"] = "/v1/pending_users";
    MODEL_PATHS["company_transactions"] = "/v1/company_transactions";
    MODEL_PATHS["comp_emp_txns_bulk"] = "/v1/comp_emp_txns/bulk";
    MODEL_PATHS["comp_emp_txns_sync"] = "/v1/comp_emp_txns/sync";
    MODEL_PATHS["comp_emp_txns"] = "/v1/comp_emp_txns";
    MODEL_PATHS["merchant_transactions"] = "/v1/merchant_transactions";
    MODEL_PATHS["merchant_cash_transactions"] = "/v1/merchant_cash_transactions";
    MODEL_PATHS["AUTH2"] = "/v2/session/challenge";
    MODEL_PATHS["forgot_password"] = "/v1/authenticate/forgotPassword";
    MODEL_PATHS["change_password"] = "/v1/authenticate/changePassword";
    MODEL_PATHS["employee_profile"] = "/v1/employees/profiles";
    MODEL_PATHS["employee_wallet"] = "/v1/employees/:empId/wallet";
})(MODEL_PATHS = exports.MODEL_PATHS || (exports.MODEL_PATHS = {}));
var GQL_PATHS;
(function (GQL_PATHS) {
    GQL_PATHS["emp_reports"] = "/graphql/empreports";
    GQL_PATHS["comp_reports"] = "/graphql/comp-reports";
    GQL_PATHS["login_service"] = "/graphql/login-service";
    GQL_PATHS["org_admin_service"] = "/graphql/org-admin-service";
})(GQL_PATHS = exports.GQL_PATHS || (exports.GQL_PATHS = {}));
