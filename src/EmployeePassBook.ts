const moment = require("moment-timezone");
import { PassBookTranaction } from "./Protocols";



export default class EmployeePassBook {

    private _mf: any
    constructor(model_factory: any) {

        this._mf = model_factory
    }



    async fetch(before_ts: number, after_ts: number): Promise<[PassBookTranaction]> {
        const auth_base_url = this._mf.api_base_url();
        let tz = 'Asia/Kolkata';
        let before_moment = before_ts ? moment(before_ts * 1000).tz(tz) : moment().tz(tz);
        let after_moment = after_ts ? moment(after_ts * 1000).tz(tz) : moment().tz(tz).subtract('30', 'days');
        let diff_in_days = before_moment.diff(after_moment, 'days');
        if (diff_in_days > 90) {
            throw new Error('Not allowed to fetch transactions for any period greater than 90 days');
        }
        let after = Math.floor(after_moment.valueOf() / 1000);
        let before = Math.floor(before_moment.valueOf() / 1000);

        let mf = this._mf;
        let txn_limit = 10000; // TODO : to be unlimited
        const date_format = 'DD-MM-YYYY hh:mm:ss';

        let comp_emp_txns = (await mf.compEmpTxns().get("", {
            'pagination.sort': 'desc',
            'pagination.limit': txn_limit,
            'pagination.after': after,
            'pagination.before': before,
            'pagination.field': 'initiated_ts'
        })).map((t: any) => {
            return {
                modified_ts: t.modified_ts,
                txn_type: t.txn_type,
                reimbursement_txn_id: t.reimbursement_txn_id,
                timestamp: t.initiated_ts,
                date: moment(t.initiated_ts * 1000).tz(tz).format(date_format),
                txnId: t._id,
                approval_status: t.approval_status,
                amount_type: t.amount_type,
                type: t.amount > 0 ? 'credit' : 'debit',
                amount: Math.abs(t.amount),
                resource_url: `${auth_base_url}/v1/comp_emp_txns/${t._id}`
            }
        })

        let merchant_txns = (await mf.merchantTxns().get("", {
            'pagination.sort': 'desc',
            'pagination.limit': txn_limit,
            'pagination.after': after_moment.format('YYYYMMDDHHmmss'),
            'pagination.before': before_moment.format('YYYYMMDDHHmmss'),
            'pagination.field': 'txnDate'
        })).map((t: any) => {
            let initiate_ts = moment(t.txnDate, 'YYYYMMDDHHmmss').tz(tz).valueOf();
            return {
                modified_ts: t.modified_ts,
                txn_type: t.amount > 0 ? 'POS' : 'POS_REVERSAL',
                mcc: t.mcc,
                merchantName: t.merchantName,
                timestamp: Math.floor(initiate_ts / 1000),
                date: moment(initiate_ts).tz(tz).format(date_format),
                txnId: t._id,
                approval_status: t.txnStatus,
                type: t.amount > 0 ? 'debit' : 'credit',
                amount: Math.abs(t.amount),
                amount_type: t.amount_type,
                resource_url: `${auth_base_url}/v1/merchant_transactions/${t._id}`
            }
        });
        let merchant_cash_txns = (await mf.anyModel('/v1/merchant_cash_transactions').get("", {
            'pagination.sort': 'desc',
            'pagination.limit': txn_limit,
            'pagination.after': after_moment.format('YYYYMMDDHHmmss'),
            'pagination.before': before_moment.format('YYYYMMDDHHmmss'),
            'pagination.field': 'txnDate'
        })).map((t: any) => {
            let initiate_ts = moment(t.txnDate, 'YYYYMMDDHHmmss').tz(tz).valueOf();

            return {
                modified_ts: t.modified_ts,

                txn_type: t.amount > 0 ? 'CASH' : 'CASH_REVERSAL',
                mcc: t.mcc,
                merchantName: t.merchantName,
                timestamp: Math.floor(initiate_ts / 1000),
                date: moment(initiate_ts).tz(tz).format(date_format),
                txnId: t._id,
                approval_status: t.txnStatus,
                type: t.amount > 0 ? 'debit' : 'credit',
                amount: Math.abs(t.amount),
                amount_type: t.amount_type,
                resource_url: `${auth_base_url}/v1/merchant_cash_transactions/${t._id}`
            }
        })
        let personal_topup = (await mf.anyModel('/v1/payment_gateway_txn').get("", {
            'pagination.sort': 'desc',
            'pagination.limit': txn_limit,
            'pagination.after': after,
            'pagination.before': before,
            'pagination.field': 'initiated_ts'
        })).map((t: any) => {
            return {
                modified_ts: t.modified_ts,
                timestamp: t.initiated_ts,
                date: moment(t.initiated_ts * 1000).tz(tz).format(date_format),
                txnId: t._id,
                approval_status: t.approval_status,
                type: t.amount > 0 ? 'credit' : 'debit',
                amount_type: t.amount_type,
                amount: Math.abs(t.amount),
                resource_url: `${auth_base_url}/v1/payment_gateway_txn/${t._id}`
            }
        })

        // console.log({ comp_emp_txns, merchant_txns, merchant_cash_txns, personal_topup });
        let txns: PassBookTranaction[] = [...comp_emp_txns, ...merchant_txns, ...merchant_cash_txns, ...personal_topup];
        txns = txns.sort((t1, t2) => {
            return t2.timestamp - t1.timestamp;
        })

        return txns as [PassBookTranaction];



    }
}