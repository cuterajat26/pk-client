/**
 * Created by rohittalwar on 10/05/16.
 */

export default class UserDefaults {

    static get(key:string):any{
        return localStorage.getItem(key)
    }
    static put(key:string,obj:any){
        localStorage.setItem(key,obj)
    }
    static remove(key:string){
        localStorage.removeItem(key)
    }
}