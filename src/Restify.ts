/**
 * Created by rohittalwar on 13/04/16.
 */

import { ModelProtocol, NetworkInterface } from "./Protocols";

class Restify implements ModelProtocol {
    base_url: string
    networkClient: NetworkInterface
    constructor(base_url: string, networkClient: NetworkInterface) {
        this.base_url = base_url
        this.networkClient = networkClient
    }

    async get(id?: string, options?: Object): Promise<any> {
        let resourceString = id ? ("/" + id) : ""
        let path = this.base_url + resourceString
        let res = await this.networkClient.get(path, options);
        return res.data;
    }
    async query(params?: Object, options?: Object): Promise<any> {

        return (await this.networkClient.get(this.base_url, params)).data;

    }
    async add(params: Object): Promise<any> {
        return (await this.networkClient.post(this.base_url, params)).data;
    }
    async put(id: string, params: Object): Promise<any> {
        let resourceString = id ? ("/" + id) : ""
        let path = this.base_url + resourceString
        return (await this.networkClient.put(path, params)).data;
    }

    async remove(id: string, options?: Object): Promise<any> {
        let path = this.base_url + "/" + id

        return (await this.networkClient.delete(path, options)).data;
    }
}

export default Restify