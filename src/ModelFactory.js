"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Restify_1 = require("./Restify");
var Networking_1 = require("./Networking");
var ApiRoutes_1 = require("./ApiRoutes");
var GQLClient_1 = require("./GQLClient");
var TenantGQLClient_1 = require("./TenantGQLClient");
var EmployeePassBook_1 = require("./EmployeePassBook");
var ModelFactory = /** @class */ (function () {
    function ModelFactory(config) {
        this._api_base_url = config.api_base_url;
        this._token = config.token || '';
    }
    ModelFactory.prototype.setToken = function (token) {
        this._token = token;
    };
    ModelFactory.prototype.gql = function (path) {
        var url = this.api_base_url() + "/ms" + path;
        return new GQLClient_1.default(url, this._token);
    };
    ModelFactory.prototype.gqlclient = function (path) {
        var url = this.api_base_url() + "/" + path;
        return new GQLClient_1.default(url, this._token);
    };
    ModelFactory.prototype.tenant_gql_client = function (path, tenant) {
        var url = this.api_base_url() + "/" + path;
        return new TenantGQLClient_1.default(url, this._token, tenant);
    };
    ModelFactory.prototype.anyModel = function (path /** ex: /v1/resource/path */) {
        var url = "" + this.api_base_url() + path;
        return new Restify_1.default(url, Networking_1.NetworkFactory.createSimpleClient(this._token));
    };
    ModelFactory.prototype.cardKit = function () {
        var url = "" + this.api_base_url() + ApiRoutes_1.MODEL_PATHS.card_kit;
        return new Restify_1.default(url, Networking_1.NetworkFactory.createSimpleClient(this._token));
    };
    ModelFactory.prototype.api_base_url = function () {
        return this._api_base_url;
    };
    ModelFactory.prototype.urlConfig = function () {
        var url = "" + this.api_base_url() + ApiRoutes_1.MODEL_PATHS.config_urls;
        return new Restify_1.default(url, Networking_1.NetworkFactory.createSimpleClient(this._token));
    };
    ModelFactory.prototype.bulkEmployees = function () {
        var url = "" + this.api_base_url() + ApiRoutes_1.MODEL_PATHS.employees_bulk;
        return new Restify_1.default(url, Networking_1.NetworkFactory.createSimpleClient(this._token));
    };
    ModelFactory.prototype.sendOtp = function (token) {
        var url = "" + this.api_base_url() + ApiRoutes_1.MODEL_PATHS.otp;
        return new Restify_1.default(url, Networking_1.NetworkFactory.createSimpleClient(this._token));
    };
    ModelFactory.prototype.sendOtpVerify = function (token) {
        var url = "" + this.api_base_url() + ApiRoutes_1.MODEL_PATHS.otp_attach;
        return new Restify_1.default(url, Networking_1.NetworkFactory.createSimpleClient(this._token));
    };
    ModelFactory.prototype.signup = function () {
        var url = "" + this.api_base_url() + ApiRoutes_1.MODEL_PATHS.signup;
        return new Restify_1.default(url, Networking_1.NetworkFactory.createSimpleClient(this._token));
    };
    ModelFactory.prototype.resendSignupVerifyEmail = function () {
        var url = "" + this.api_base_url() + ApiRoutes_1.MODEL_PATHS.signup_email_verify;
        return new Restify_1.default(url, Networking_1.NetworkFactory.createSimpleClient(this._token));
    };
    ModelFactory.prototype.resendVerifyEmail = function (emp) {
        var url = this.api_base_url() + "/v1/employees/" + emp + "/resend_verification";
        return new Restify_1.default(url, Networking_1.NetworkFactory.createSimpleClient(this._token));
    };
    ModelFactory.prototype.resendOTPEmail = function (emp) {
        var url = this.api_base_url() + "/v1/employees/" + emp + "/resend_otp_email";
        return new Restify_1.default(url, Networking_1.NetworkFactory.createSimpleClient(this._token));
    };
    ModelFactory.prototype.verifyEmail = function (token) {
        var url = "" + this.api_base_url() + ApiRoutes_1.MODEL_PATHS.email_verify;
        return new Restify_1.default(url, Networking_1.NetworkFactory.createSimpleClient(this._token));
    };
    ModelFactory.prototype.loggedInUser = function () {
        var url = "" + this.api_base_url() + ApiRoutes_1.MODEL_PATHS.users;
        return new Restify_1.default(url, Networking_1.NetworkFactory.createSimpleClient(this._token));
    };
    ModelFactory.prototype.pendingUsers = function () {
        var url = "" + this.api_base_url() + ApiRoutes_1.MODEL_PATHS.pending_users;
        return new Restify_1.default(url, Networking_1.NetworkFactory.createSimpleClient(this._token));
    };
    ModelFactory.prototype.compTransactions = function () {
        var url = "" + this.api_base_url() + ApiRoutes_1.MODEL_PATHS.company_transactions;
        return new Restify_1.default(url, Networking_1.NetworkFactory.createSimpleClient(this._token));
    };
    ModelFactory.prototype.bulkCompEmpTxns = function () {
        var url = "" + this.api_base_url() + ApiRoutes_1.MODEL_PATHS.comp_emp_txns_bulk;
        return new Restify_1.default(url, Networking_1.NetworkFactory.createSimpleClient(this._token));
    };
    ModelFactory.prototype.compEmpTxnSync = function () {
        var url = "" + this.api_base_url() + ApiRoutes_1.MODEL_PATHS.comp_emp_txns_sync;
        return new Restify_1.default(url, Networking_1.NetworkFactory.createSimpleClient(this._token));
    };
    ModelFactory.prototype.compEmpTxns = function () {
        var url = "" + this.api_base_url() + ApiRoutes_1.MODEL_PATHS.comp_emp_txns;
        return new Restify_1.default(url, Networking_1.NetworkFactory.createSimpleClient(this._token));
    };
    ModelFactory.prototype.merchantTxns = function () {
        var url = "" + this.api_base_url() + ApiRoutes_1.MODEL_PATHS.merchant_transactions;
        return new Restify_1.default(url, Networking_1.NetworkFactory.createSimpleClient(this._token));
    };
    ModelFactory.prototype.merchantCashTxns = function () {
        var url = "" + this.api_base_url() + ApiRoutes_1.MODEL_PATHS.merchant_cash_transactions;
        return new Restify_1.default(url, Networking_1.NetworkFactory.createSimpleClient(this._token));
    };
    ModelFactory.prototype.compWallet = function (comp_id) {
        var url = this.api_base_url() + "/v1/companies/" + comp_id + "/wallet";
        return new Restify_1.default(url, Networking_1.NetworkFactory.createSimpleClient(this._token));
    };
    ModelFactory.prototype.empWallet = function (emp_id) {
        var url = this.api_base_url() + "/v1/employees/" + emp_id + "/wallet";
        return new Restify_1.default(url, Networking_1.NetworkFactory.createSimpleClient(this._token));
    };
    ModelFactory.prototype.companies = function () {
        var url = "" + this.api_base_url() + ApiRoutes_1.MODEL_PATHS.companies;
        return new Restify_1.default(url, Networking_1.NetworkFactory.createSimpleClient(this._token));
    };
    ModelFactory.prototype.employees = function () {
        var url = "" + this.api_base_url() + ApiRoutes_1.MODEL_PATHS.employees;
        return new Restify_1.default(url, Networking_1.NetworkFactory.createSimpleClient(this._token));
    };
    ModelFactory.prototype.passbookAdapter = function () {
        return (new EmployeePassBook_1.default(this));
    };
    return ModelFactory;
}());
exports.default = ModelFactory;
