"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var moment = require("moment-timezone");
var EmployeePassBook = /** @class */ (function () {
    function EmployeePassBook(model_factory) {
        this._mf = model_factory;
    }
    EmployeePassBook.prototype.fetch = function (before_ts, after_ts) {
        return __awaiter(this, void 0, void 0, function () {
            var auth_base_url, tz, before_moment, after_moment, diff_in_days, after, before, mf, txn_limit, date_format, comp_emp_txns, merchant_txns, merchant_cash_txns, personal_topup, txns;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        auth_base_url = this._mf.api_base_url();
                        tz = 'Asia/Kolkata';
                        before_moment = before_ts ? moment(before_ts * 1000).tz(tz) : moment().tz(tz);
                        after_moment = after_ts ? moment(after_ts * 1000).tz(tz) : moment().tz(tz).subtract('30', 'days');
                        diff_in_days = before_moment.diff(after_moment, 'days');
                        if (diff_in_days > 90) {
                            throw new Error('Not allowed to fetch transactions for any period greater than 90 days');
                        }
                        after = Math.floor(after_moment.valueOf() / 1000);
                        before = Math.floor(before_moment.valueOf() / 1000);
                        mf = this._mf;
                        txn_limit = 10000;
                        date_format = 'DD-MM-YYYY hh:mm:ss';
                        return [4 /*yield*/, mf.compEmpTxns().get("", {
                                'pagination.sort': 'desc',
                                'pagination.limit': txn_limit,
                                'pagination.after': after,
                                'pagination.before': before,
                                'pagination.field': 'initiated_ts'
                            })];
                    case 1:
                        comp_emp_txns = (_a.sent()).map(function (t) {
                            return {
                                modified_ts: t.modified_ts,
                                txn_type: t.txn_type,
                                reimbursement_txn_id: t.reimbursement_txn_id,
                                timestamp: t.initiated_ts,
                                date: moment(t.initiated_ts * 1000).tz(tz).format(date_format),
                                txnId: t._id,
                                approval_status: t.approval_status,
                                amount_type: t.amount_type,
                                type: t.amount > 0 ? 'credit' : 'debit',
                                amount: Math.abs(t.amount),
                                resource_url: auth_base_url + "/v1/comp_emp_txns/" + t._id
                            };
                        });
                        return [4 /*yield*/, mf.merchantTxns().get("", {
                                'pagination.sort': 'desc',
                                'pagination.limit': txn_limit,
                                'pagination.after': after_moment.format('YYYYMMDDHHmmss'),
                                'pagination.before': before_moment.format('YYYYMMDDHHmmss'),
                                'pagination.field': 'txnDate'
                            })];
                    case 2:
                        merchant_txns = (_a.sent()).map(function (t) {
                            var initiate_ts = moment(t.txnDate, 'YYYYMMDDHHmmss').tz(tz).valueOf();
                            return {
                                modified_ts: t.modified_ts,
                                txn_type: t.amount > 0 ? 'POS' : 'POS_REVERSAL',
                                mcc: t.mcc,
                                merchantName: t.merchantName,
                                timestamp: Math.floor(initiate_ts / 1000),
                                date: moment(initiate_ts).tz(tz).format(date_format),
                                txnId: t._id,
                                approval_status: t.txnStatus,
                                type: t.amount > 0 ? 'debit' : 'credit',
                                amount: Math.abs(t.amount),
                                amount_type: t.amount_type,
                                resource_url: auth_base_url + "/v1/merchant_transactions/" + t._id
                            };
                        });
                        return [4 /*yield*/, mf.anyModel('/v1/merchant_cash_transactions').get("", {
                                'pagination.sort': 'desc',
                                'pagination.limit': txn_limit,
                                'pagination.after': after_moment.format('YYYYMMDDHHmmss'),
                                'pagination.before': before_moment.format('YYYYMMDDHHmmss'),
                                'pagination.field': 'txnDate'
                            })];
                    case 3:
                        merchant_cash_txns = (_a.sent()).map(function (t) {
                            var initiate_ts = moment(t.txnDate, 'YYYYMMDDHHmmss').tz(tz).valueOf();
                            return {
                                modified_ts: t.modified_ts,
                                txn_type: t.amount > 0 ? 'CASH' : 'CASH_REVERSAL',
                                mcc: t.mcc,
                                merchantName: t.merchantName,
                                timestamp: Math.floor(initiate_ts / 1000),
                                date: moment(initiate_ts).tz(tz).format(date_format),
                                txnId: t._id,
                                approval_status: t.txnStatus,
                                type: t.amount > 0 ? 'debit' : 'credit',
                                amount: Math.abs(t.amount),
                                amount_type: t.amount_type,
                                resource_url: auth_base_url + "/v1/merchant_cash_transactions/" + t._id
                            };
                        });
                        return [4 /*yield*/, mf.anyModel('/v1/payment_gateway_txn').get("", {
                                'pagination.sort': 'desc',
                                'pagination.limit': txn_limit,
                                'pagination.after': after,
                                'pagination.before': before,
                                'pagination.field': 'initiated_ts'
                            })];
                    case 4:
                        personal_topup = (_a.sent()).map(function (t) {
                            return {
                                modified_ts: t.modified_ts,
                                timestamp: t.initiated_ts,
                                date: moment(t.initiated_ts * 1000).tz(tz).format(date_format),
                                txnId: t._id,
                                approval_status: t.approval_status,
                                type: t.amount > 0 ? 'credit' : 'debit',
                                amount_type: t.amount_type,
                                amount: Math.abs(t.amount),
                                resource_url: auth_base_url + "/v1/payment_gateway_txn/" + t._id
                            };
                        });
                        txns = comp_emp_txns.concat(merchant_txns, merchant_cash_txns, personal_topup);
                        txns = txns.sort(function (t1, t2) {
                            return t2.timestamp - t1.timestamp;
                        });
                        return [2 /*return*/, txns];
                }
            });
        });
    };
    return EmployeePassBook;
}());
exports.default = EmployeePassBook;
