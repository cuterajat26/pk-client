/**
 * Created by rohittalwar on 13/06/16.
 */
export default class AppGlobals {

    static data: any = {}
    static key_loggedin_user = "key_loggedin_user"
    static key_access_token = "key_access_token"

}