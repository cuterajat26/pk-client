import { NetworkFactory } from './Networking';
import { build_mutation_query, build_query } from './gqlquery/common';
 interface GQLClientInterface {
  query(operation: string, variables: any, fields: string): Promise<any>
}
export default class TenantGQLClient implements GQLClientInterface {
  private _url: string
  private _tenant: string
  private _token: string
  constructor(url: string, token:string,tenant: string) {
    this._url = url;
    this._tenant = tenant;
    this._token = token;
  }
  
  async query(operation: string, variables: any, fields: string): Promise<any> {
    let q = await build_query(operation, variables, fields);
    return this.network_request(q);
  }

  async mutation(operation: string, variables: any, fields: string): Promise<any> {
    let q = await build_mutation_query(operation, variables, fields);
    return this.network_request(q);
  }
  private async network_request(q: string) {
    let net = NetworkFactory.createTenantClient(this._token,this._tenant);
    let result = await net.post(this._url, { query: q });
    return result.data;
  }


}