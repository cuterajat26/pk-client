/**
 * Created by rohittalwar on 13/04/16.
 */

import Axios, * as axios from "axios";
import { NetworkInterface, CacheStoreProtocol } from "./Protocols";

class NullCacheStore implements CacheStoreProtocol {
  put(path: string, data: any): Promise<any> {
    return Promise.resolve({});
  }
  get(path: string): Promise<any> {
    return Promise.resolve({});
  }
}
export class MyNetworkClient implements NetworkInterface {
  private cacheStore: CacheStoreProtocol;
  isReactive: boolean = false;

  private headers: any = {};
  constructor() {
    this.cacheStore = new NullCacheStore();
  }
  setCacheStore(cacheStore: CacheStoreProtocol) {
    this.cacheStore = cacheStore;
  }
  setHttpHeaders(headersMap: any) {
    for (let key in headersMap) {
      this.headers[key] = headersMap[key];
    }
  }
  private _net(): axios.AxiosInstance {
    return Axios.create({
      timeout: 30000,
      headers: {
        ...this.headers,
      },
    });
  }
  private getLocation(url: string): { pathname: string } {
    try {
      var l: any = document.createElement("a");
      l.href = url;
      return l;
    } catch (E) {
      return { pathname: url };
    }
  }

  private _getCacheUrlKey(
    url: string,
    params?: { [key: string]: any }
  ): string {
    let resourcePath = this.getLocation(url).pathname;
    params = params || {};
    let str: string =
      resourcePath + (Object.keys(params).length > 0 ? "?" : "");
    for (var key in params) {
      str = str + "&" + key + "=" + encodeURIComponent(params[key] as string);
    }
    return str;
  }
  private async getDataFromCache(url: string, params?: Object): Promise<any> {
    let cahceUrl = this._getCacheUrlKey(url, params);

    return new Promise((resolve, reject) => {
      let isCacheStoreAvailable = !(this.cacheStore instanceof NullCacheStore);
      isCacheStoreAvailable
        ? this.cacheStore.get(cahceUrl).then(
            (cache) => {
              cache.data.refresh = this.isReactive
                ? this.networkGET(url, params)
                : Promise.resolve(cache.data);

              resolve(cache.data);
            },
            (err) => {
              this.networkGET(url, params).then(resolve, reject);
            }
          )
        : this.networkGET(url, params).then(resolve, reject);
    });
  }
  private async networkGET(url: string, params?: Object): Promise<any> {
    return this._net().get(url, { params });
  }
  async get(url: string, params?: Object): Promise<any> {
    return this.getDataFromCache(url, params);
  }

  async post(url: string, params?: Object): Promise<any> {
    return this._net().post(url, params);
  }

  async put(url: string, params?: Object): Promise<any> {
    return this._net().put(url, params);
  }

  async delete(url: string, params?: Object): Promise<any> {
    return this._net().put(url, params);
  }
}

export class NetworkFactory {
  static createOfflineClient(reactive: boolean = false): NetworkInterface {
    let net = NetworkFactory._createSimpleClient();
    net.isReactive = true;
    // net.setCacheStore(new CacheStore())
    return net;
  }
  static createOfflineReactiveClient(): NetworkInterface {
    let net = NetworkFactory._createSimpleClient();
    // net.setCacheStore(new CacheStore())
    net.isReactive = true;
    return net;
  }

  static createSimpleClient(token?: string): NetworkInterface {
    return NetworkFactory._createSimpleClient(token);
  }

  private static _createSimpleClient(token?: string): MyNetworkClient {
    let net = new MyNetworkClient();
    token &&
      net.setHttpHeaders({
        "x-access-token": token,
      });
    return net;
  }

  static createTenantClient(token: string, tenant: string): NetworkInterface {
    let net = new MyNetworkClient();
    net.setHttpHeaders({
      "x-m2p-tenant": tenant,
      "x-access-token": token,
    });
    return net;
  }
}
