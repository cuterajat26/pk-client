import Restify from "./Restify";
import { NetworkFactory } from "./Networking";
import { MODEL_PATHS, GQL_PATHS } from './ApiRoutes';
import GQLClient from './GQLClient';
import TenantGQLClient from './TenantGQLClient';

import EmployeePassBook from "./EmployeePassBook";


export interface ModelFactoryConfig {
    api_base_url: string
    token?: string

}
class ModelFactory {
    private _api_base_url: string;
    private _token: string;
    constructor(config: ModelFactoryConfig) {
        this._api_base_url = config.api_base_url;
        this._token = config.token || '';
    }
    setToken(token: string) {
        this._token = token;
    }

    gql(path: GQL_PATHS): GQLClient {
        let url = `${this.api_base_url()}/ms${path}`;
        return new GQLClient(url, this._token);
    }
    gqlclient(path: string): GQLClient {
        let url = `${this.api_base_url()}/${path}`;
        return new GQLClient(url, this._token);
    }
    tenant_gql_client(path: string,tenant:string): TenantGQLClient {
        let url = `${this.api_base_url()}/${path}`;
        return new TenantGQLClient(url, this._token, tenant);
    }
    anyModel(path: string /** ex: /v1/resource/path */): Restify {
        let url = `${this.api_base_url()}${path}`
        return new Restify(url, NetworkFactory.createSimpleClient(this._token))
    }
    cardKit(): Restify {

        let url = `${this.api_base_url()}${MODEL_PATHS.card_kit}`
        return new Restify(url, NetworkFactory.createSimpleClient(this._token))
    }

    api_base_url(): string {
        return this._api_base_url;

    }

    urlConfig(): Restify {
        let url = `${this.api_base_url()}${MODEL_PATHS.config_urls}`
        return new Restify(url, NetworkFactory.createSimpleClient(this._token))

    }
    bulkEmployees(): Restify {

        let url = `${this.api_base_url()}${MODEL_PATHS.employees_bulk}`

        return new Restify(url, NetworkFactory.createSimpleClient(this._token))
    }

    sendOtp(token?: string): Restify {

        let url = `${this.api_base_url()}${MODEL_PATHS.otp}`
        return new Restify(url, NetworkFactory.createSimpleClient(this._token))

    }
    sendOtpVerify(token?: string): Restify {

        let url = `${this.api_base_url()}${MODEL_PATHS.otp_attach}`
        return new Restify(url, NetworkFactory.createSimpleClient(this._token))

    }
    signup(): Restify {

        let url = `${this.api_base_url()}${MODEL_PATHS.signup}`

        return new Restify(url, NetworkFactory.createSimpleClient(this._token))
    }
    resendSignupVerifyEmail(): Restify {

        let url = `${this.api_base_url()}${MODEL_PATHS.signup_email_verify}`

        return new Restify(url, NetworkFactory.createSimpleClient(this._token))
    }
    resendVerifyEmail(emp: string): Restify {

        let url = `${this.api_base_url()}/v1/employees/${emp}/resend_verification`

        return new Restify(url, NetworkFactory.createSimpleClient(this._token))
    }
    resendOTPEmail(emp: string): Restify {

        let url = `${this.api_base_url()}/v1/employees/${emp}/resend_otp_email`

        return new Restify(url, NetworkFactory.createSimpleClient(this._token))
    }


    verifyEmail(token?: string): Restify {

        let url = `${this.api_base_url()}${MODEL_PATHS.email_verify}`

        return new Restify(url, NetworkFactory.createSimpleClient(this._token))
    }




    loggedInUser(): Restify {

        let url = `${this.api_base_url()}${MODEL_PATHS.users}`
        return new Restify(url, NetworkFactory.createSimpleClient(this._token))
    }

    pendingUsers(): Restify {
        let url = `${this.api_base_url()}${MODEL_PATHS.pending_users}`

        return new Restify(url, NetworkFactory.createSimpleClient(this._token))
    }

    compTransactions(): Restify {
        let url = `${this.api_base_url()}${MODEL_PATHS.company_transactions}`

        return new Restify(url, NetworkFactory.createSimpleClient(this._token))
    }
    bulkCompEmpTxns(): Restify {
        let url = `${this.api_base_url()}${MODEL_PATHS.comp_emp_txns_bulk}`

        return new Restify(url, NetworkFactory.createSimpleClient(this._token))
    }
    compEmpTxnSync(): Restify {
        let url = `${this.api_base_url()}${MODEL_PATHS.comp_emp_txns_sync}`

        return new Restify(url, NetworkFactory.createSimpleClient(this._token))
    }

    compEmpTxns(): Restify {
        let url = `${this.api_base_url()}${MODEL_PATHS.comp_emp_txns}`

        return new Restify(url, NetworkFactory.createSimpleClient(this._token))
    }

    merchantTxns(): Restify {
        let url = `${this.api_base_url()}${MODEL_PATHS.merchant_transactions}`;
        return new Restify(url, NetworkFactory.createSimpleClient(this._token))

    }

    merchantCashTxns(): Restify {
        let url = `${this.api_base_url()}${MODEL_PATHS.merchant_cash_transactions}`;
        return new Restify(url, NetworkFactory.createSimpleClient(this._token))

    }

    compWallet(comp_id: string): Restify {

        let url = `${this.api_base_url()}/v1/companies/${comp_id}/wallet`

        return new Restify(url, NetworkFactory.createSimpleClient(this._token))
    }
    empWallet(emp_id: string): Restify {

        let url = `${this.api_base_url()}/v1/employees/${emp_id}/wallet`

        return new Restify(url, NetworkFactory.createSimpleClient(this._token))
    }
    companies(): Restify {

        let url = `${this.api_base_url()}${MODEL_PATHS.companies}`

        return new Restify(url, NetworkFactory.createSimpleClient(this._token))
    }

    employees(): Restify {

        let url = `${this.api_base_url()}${MODEL_PATHS.employees}`

        return new Restify(url, NetworkFactory.createSimpleClient(this._token))
    }
    passbookAdapter(): EmployeePassBook {
        return (new EmployeePassBook(this))
    }

}

export default ModelFactory