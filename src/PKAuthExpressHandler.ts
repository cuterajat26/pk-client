
import * as assert from "assert"
import ModelFactory from "./ModelFactory";
import { MODEL_PATHS } from "./ApiRoutes";
export default function (base_url: string) {
    return async (req: any, res: any, next: any) => {

        // check header or url parameters or post parameters for token
        var token = req.body.token || req.query.token || req.headers['x-access-token'];
        req.headers['x-access-token'] = token;
        req.locals = {};
        req.locals.token = res.locals.token = token;
        delete req.query.token;
        const mf = new ModelFactory({ api_base_url: base_url, token: token })
        try {
            assert.ok(token, 'Failed to retrieve token.');
            let user = null;
            let system_user = null;
            try {
                user = await mf.anyModel(MODEL_PATHS.users).get('me');
            } catch (err) {
                user = null;
                //continue;
            }

            try {
                system_user = await mf.anyModel(MODEL_PATHS.users).get('system');
            } catch (err) {
                system_user = null;
                //continue;
            }

            assert.ok(user || system_user, 'Failed to authenticate token for user/system.');
            req.locals.user = res.locals.user = (user || system_user);
            next();
        } catch (err) {
            res.status(403);
            next(err);

        }
    }
}