"use strict";
/**
 * Created by rohittalwar on 13/04/16.
 */
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
var NullCacheStore = /** @class */ (function () {
    function NullCacheStore() {
    }
    NullCacheStore.prototype.put = function (path, data) {
        return Promise.resolve({});
    };
    NullCacheStore.prototype.get = function (path) {
        return Promise.resolve({});
    };
    return NullCacheStore;
}());
var MyNetworkClient = /** @class */ (function () {
    function MyNetworkClient() {
        this.isReactive = false;
        this.headers = {};
        this.cacheStore = new NullCacheStore();
    }
    MyNetworkClient.prototype.setCacheStore = function (cacheStore) {
        this.cacheStore = cacheStore;
    };
    MyNetworkClient.prototype.setHttpHeaders = function (headersMap) {
        for (var key in headersMap) {
            this.headers[key] = headersMap[key];
        }
    };
    MyNetworkClient.prototype._net = function () {
        return axios_1.default.create({
            timeout: 30000,
            headers: __assign({}, this.headers),
        });
    };
    MyNetworkClient.prototype.getLocation = function (url) {
        try {
            var l = document.createElement("a");
            l.href = url;
            return l;
        }
        catch (E) {
            return { pathname: url };
        }
    };
    MyNetworkClient.prototype._getCacheUrlKey = function (url, params) {
        var resourcePath = this.getLocation(url).pathname;
        params = params || {};
        var str = resourcePath + (Object.keys(params).length > 0 ? "?" : "");
        for (var key in params) {
            str = str + "&" + key + "=" + encodeURIComponent(params[key]);
        }
        return str;
    };
    MyNetworkClient.prototype.getDataFromCache = function (url, params) {
        return __awaiter(this, void 0, void 0, function () {
            var cahceUrl;
            var _this = this;
            return __generator(this, function (_a) {
                cahceUrl = this._getCacheUrlKey(url, params);
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        var isCacheStoreAvailable = !(_this.cacheStore instanceof NullCacheStore);
                        isCacheStoreAvailable
                            ? _this.cacheStore.get(cahceUrl).then(function (cache) {
                                cache.data.refresh = _this.isReactive
                                    ? _this.networkGET(url, params)
                                    : Promise.resolve(cache.data);
                                resolve(cache.data);
                            }, function (err) {
                                _this.networkGET(url, params).then(resolve, reject);
                            })
                            : _this.networkGET(url, params).then(resolve, reject);
                    })];
            });
        });
    };
    MyNetworkClient.prototype.networkGET = function (url, params) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this._net().get(url, { params: params })];
            });
        });
    };
    MyNetworkClient.prototype.get = function (url, params) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.getDataFromCache(url, params)];
            });
        });
    };
    MyNetworkClient.prototype.post = function (url, params) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this._net().post(url, params)];
            });
        });
    };
    MyNetworkClient.prototype.put = function (url, params) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this._net().put(url, params)];
            });
        });
    };
    MyNetworkClient.prototype.delete = function (url, params) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this._net().put(url, params)];
            });
        });
    };
    return MyNetworkClient;
}());
exports.MyNetworkClient = MyNetworkClient;
var NetworkFactory = /** @class */ (function () {
    function NetworkFactory() {
    }
    NetworkFactory.createOfflineClient = function (reactive) {
        if (reactive === void 0) { reactive = false; }
        var net = NetworkFactory._createSimpleClient();
        net.isReactive = true;
        // net.setCacheStore(new CacheStore())
        return net;
    };
    NetworkFactory.createOfflineReactiveClient = function () {
        var net = NetworkFactory._createSimpleClient();
        // net.setCacheStore(new CacheStore())
        net.isReactive = true;
        return net;
    };
    NetworkFactory.createSimpleClient = function (token) {
        return NetworkFactory._createSimpleClient(token);
    };
    NetworkFactory._createSimpleClient = function (token) {
        var net = new MyNetworkClient();
        token &&
            net.setHttpHeaders({
                "x-access-token": token,
            });
        return net;
    };
    NetworkFactory.createTenantClient = function (token, tenant) {
        var net = new MyNetworkClient();
        net.setHttpHeaders({
            "x-m2p-tenant": tenant,
            "x-access-token": token,
        });
        return net;
    };
    return NetworkFactory;
}());
exports.NetworkFactory = NetworkFactory;
