import { Buffer } from "buffer";
const crypto: any = require("crypto-browserify");
import ModelFactory from "./ModelFactory";
import { MODEL_PATHS } from "./ApiRoutes";

export interface SessionChallengeInterface {
  login(): Promise<any>;
}

export default class SessionChallenge implements SessionChallengeInterface {
  private _url: string;
  private _username: string;
  private _password: string;
  constructor(base_url: string, username: string, password: string) {
    this._url = base_url;
    this._username = username;
    this._password = password;
  }

  async login(): Promise<any> {
    let mf = new ModelFactory({ api_base_url: this._url });
    try {
      let session = await mf.anyModel(MODEL_PATHS.AUTH2).get(this._username);
      if (session.error) return session.error;
      if (session.details) {
        let secret = session.details.pow_secret;
        let salt = Buffer.from(session.details.pow_salt, "hex");
        let prefix = session.details.pow_hash_prefix;
        let key = await this.guessKey(
          secret,
          salt,
          prefix,
          session.details.pow_rounds,
          session.details.key_length
        );
        let iv = crypto.randomBytes(16);
        let encryptedPass = await this.encryptPassword(this._password, key, iv);
        let body = {
          refNo: iv.toString("hex"),
          id: session._id,
          password: encryptedPass,
        };
        let result = mf
          .anyModel(MODEL_PATHS.AUTH2 + "/" + this._username)
          .add(body);
        return result;
      }
    } catch (e) {
      throw e;
    }
  }

  private async encryptPassword(
    password: string,
    key: Buffer,
    iv: Buffer
  ): Promise<string> {
    let cipherName = "aes-256-cbc";

    let cipher = crypto.createCipheriv(cipherName, key, iv);
    cipher.setAutoPadding(true);
    let crypted = cipher.update(password, "utf8", "hex");
    crypted += cipher.final("hex");
    return crypted;
  }

  private async guessKey(
    secretStr: string,
    salt: Buffer,
    prefix: string,
    powRounds: number,
    keyLen: number
  ): Promise<any> {
    let secret = Number(secretStr);
    let found = false;
    let key;
    let i = 0;
    for (i = 0; i < 200 && found == false; i++) {
      key = await this.mypbkdf2(
        `${secret + i}`,
        salt,
        powRounds,
        keyLen,
        "sha512"
      );
      found = key.toString("hex").indexOf(prefix) === 0;
    }
    !found &&
      (() => {
        throw new Error("Max guess limit Reached.Key could not be derived");
      })();

    return key;
  }

  private mypbkdf2(
    pwd: string,
    salt: Buffer,
    powRounds: number,
    keyLen: number,
    hash: string
  ): Promise<Buffer> {
    return new Promise((resolve, reject) => {
      crypto.pbkdf2(
        pwd,
        salt,
        powRounds,
        keyLen,
        hash,
        (err: Error, key: Buffer) => {
          if (err) {
            reject(err);
            return;
          }
          resolve(key);
        }
      );
    });
  }
}
