"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by rohittalwar on 13/06/16.
 */
var AppGlobals = /** @class */ (function () {
    function AppGlobals() {
    }
    AppGlobals.data = {};
    AppGlobals.key_loggedin_user = "key_loggedin_user";
    AppGlobals.key_access_token = "key_access_token";
    return AppGlobals;
}());
exports.default = AppGlobals;
