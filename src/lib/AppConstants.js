"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Roles = {
    internal_api: "internal_api",
    admin: "admin",
    merchant: "merchant",
    employee: "employee",
    company: "company",
    demo_company: "demo-company",
    demo_employee: "demo-employee",
    account_reviewer: "account_reviewer",
    claim_reviewer: "claim_reviewer",
    claim_settler: "claim_settler",
    txn_archiver: "txn_archiver"
};
exports.FundsType = {
    food: "food",
    fuel: "fuel",
    expense: "expense"
};
exports.TransactionStatus = {
    company_reviewed: "company_reviewed",
    company_approved: "company_approved",
    company_disapproved: "company_disapproved",
    internally_approved: "internally_approved",
    disapproved: "disapproved",
    pending_wallet_limit: "pending_wallet_limit",
    pending_submission: "pending_submission",
    pending_approval: "pending_approval",
    pending_kit_sync: "pending_kit_sync",
    failed_kit_sync: "failed_kit_sync",
    approved: "approved",
    externally_approved: "externally_approved",
    externally_disapproved: "externally_disapproved",
    internally_approved_no_kit_attached: "internally_approved_no_kit_attached",
    cbs_rollback_internally_approved: "cbs_rollback_internally_approved",
    cbs_rollback_approved: "cbs_rollback_approved",
};
