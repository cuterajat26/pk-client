export default function extract_token_from_request(req: any): string {
    var token = (req.locals && req.locals.token) || req.query.token || req.body.token || req.headers['x-access-token'];
    return token;
}