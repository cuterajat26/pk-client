import * as assert from "assert"
import extract_token_from_request from "./Common";
import ModelFactory from "../ModelFactory";
import { MODEL_PATHS } from "../ApiRoutes";





function gql_force_company_argument(base_url: string) {
    return function (
        target: Object,
        propertyName: string,
        propertyDesciptor: PropertyDescriptor): PropertyDescriptor {
        // target === Employee.prototype
        // propertyName === "greet"
        // propertyDesciptor === Object.getOwnPropertyDescriptor(Employee.prototype, "greet")
        const method = propertyDesciptor.value;

        propertyDesciptor.value = async function (...args: any[]) {
            assert.ok(args && args.length >= 2, 'AnForceCompanyArgumentService: Invalid number of arguments for the request handler');
            let req = args[1];

            var token = extract_token_from_request(req);
            req.locals = req.locals || {};
            req.locals.token = token;
            delete req.query.token;

            const mf = new ModelFactory({ api_base_url: base_url, token: token })


            let emp = await (await mf.anyModel(MODEL_PATHS.employees).get('me'));
            args[0].company = emp.company;
            const result = method.apply(this, args);

            return result;
        }
        return propertyDesciptor;
    };
}

export let AnForceCompanyArgumentService = gql_force_company_argument;