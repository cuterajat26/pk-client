import * as assert from "assert"

import extract_token_from_request from "./Common";
import ModelFactory from "../ModelFactory";




function fetch_loggedin_member(base_url: string) {
    return function (
        target: Object,
        propertyName: string,
        propertyDesciptor: PropertyDescriptor): PropertyDescriptor {
        // target === Employee.prototype
        // propertyName === "greet"
        // propertyDesciptor === Object.getOwnPropertyDescriptor(Employee.prototype, "greet")
        const method = propertyDesciptor.value;

        propertyDesciptor.value = async function (...args: any[]) {
            assert.ok(args && args.length >= 2, 'AnFetchLoggedInMember: Invalid number of arguments for the request handler');

            let req = args[1];



            var token = extract_token_from_request(req);
            req.locals = req.locals || {};
            req.locals.token = token;
            delete req.query.token;
            const mf = new ModelFactory({ api_base_url: base_url, token: token })
            assert.ok(token, 'AnFetchLoggedInMember: Failed to authenticate token.');
            let member = await mf.employees().get('me');
            assert.ok(member && member._id, 'Member not found');
            args.push(member);
            const result = method.apply(this, args);

            return result;
        }
        return propertyDesciptor;
    };
}

function fetch_member_from_argument(base_url: string, arg_name: string) {
    return function (
        target: Object,
        propertyName: string,
        propertyDesciptor: PropertyDescriptor): PropertyDescriptor {
        // target === Employee.prototype
        // propertyName === "greet"
        // propertyDesciptor === Object.getOwnPropertyDescriptor(Employee.prototype, "greet")
        const method = propertyDesciptor.value;

        propertyDesciptor.value = async function (...args: any[]) {
            assert.ok(args && args.length >= 2, 'AnFetchMemberFromArgument: Invalid number of arguments for the request handler');
            let req_args = args[0];
            let req = args[1];


            var token = extract_token_from_request(req);
            req.locals = req.locals || {};
            req.locals.token = token;
            delete req.query.token;
            const mf = new ModelFactory({ api_base_url: base_url, token: token })
            assert.ok(token, 'AnFetchMemberFromArgument: Failed to authenticate token.');
            let member = await mf.employees().get(req_args[arg_name]);
            assert.ok(member && member._id, 'Member not found');
            args.push(member);
            const result = method.apply(this, args);

            return result;
        }
        return propertyDesciptor;
    };
}

function fetch_member_from_kitNo(base_url: string, arg_name: string) {
    return function (
        target: Object,
        propertyName: string,
        propertyDesciptor: PropertyDescriptor): PropertyDescriptor {
        // target === Employee.prototype
        // propertyName === "greet"
        // propertyDesciptor === Object.getOwnPropertyDescriptor(Employee.prototype, "greet")
        const method = propertyDesciptor.value;

        propertyDesciptor.value = async function (...args: any[]) {
            assert.ok(args && args.length >= 2, 'AnFetchMemberFromKitNo: Invalid number of arguments for the request handler');
            let req_args = args[0];
            let req = args[1];
            let isDbCall = base_url === 'database';


            var token = extract_token_from_request(req);
            req.locals = req.locals || {};
            req.locals.token = token;
            delete req.query.token;
            const mf = new ModelFactory({ api_base_url: base_url, token: token })
            assert.ok(token, 'AnFetchMemberFromKitNo: Failed to authenticate token.');
            let kit;
            try {
                kit = await mf.cardKit().query({ kitNo: req_args[arg_name] });
            } catch (e) {
                throw new Error(JSON.stringify(e.response.data));
            }
            kit = isDbCall ? kit : (kit.length ? kit[0] : null);
            assert.ok(kit, 'AnFetchMemberFromKitNo: Kit not found');

            let member;
            try {
                member = await mf.employees().get(kit.employee);
            } catch (e) {
                throw new Error(JSON.stringify(e.response.data));
            }

            assert.ok(member && member._id, 'Member not found');
            args.push(member);
            const result = method.apply(this, args);

            return result;
        }
        return propertyDesciptor;
    };
}
export let AnFetchMemberFromKitNo = fetch_member_from_kitNo;
export let AnFetchMemberFromArgument = fetch_member_from_argument;
export let AnFetchLoggedInMember = fetch_loggedin_member;