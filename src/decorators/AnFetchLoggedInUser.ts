import * as assert from "assert"
import ModelFactory from "../ModelFactory";
import extract_token_from_request from "./Common";


function fetch_loggedin_user(base_url: string) {
    return function (
        target: Object,
        propertyName: string,
        propertyDesciptor: PropertyDescriptor): PropertyDescriptor {
        // target === Employee.prototype
        // propertyName === "greet"
        // propertyDesciptor === Object.getOwnPropertyDescriptor(Employee.prototype, "greet")
        const method = propertyDesciptor.value;

        propertyDesciptor.value = async function (...args: any[]) {
            assert.ok(args && args.length >= 2, 'AnFetchLoggedInUser: Invalid number of arguments for the request handler');

            let req = args[1];



            var token = extract_token_from_request(req);
            req.locals = req.locals || {};
            req.locals.token = token;
            delete req.query.token;
            const mf = new ModelFactory({ api_base_url: base_url, token: token })
            assert.ok(token, 'AnFetchLoggedInUser: Failed to authenticate token.');
            let user = req.locals.user || (await mf.loggedInUser().get('me'));
            assert.ok(user, 'User not found');
            args.push(user);
            const result = method.apply(this, args);

            return result;
        }
        return propertyDesciptor;
    };
}

export let AnFetchLoggedInUser = fetch_loggedin_user;