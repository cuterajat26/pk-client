import * as assert from "assert"
import ModelFactory from "../ModelFactory";
import extract_token_from_request from "./Common";
import { MODEL_PATHS } from "../ApiRoutes";


function intersection(a: any[] = [], b: any[] = []): number {
    let count = 0;

    for (let i of a) {
        count = count + (b.indexOf(i) >= 0 ? 1 : 0);
    }
    return count;
}
function gql_role_access(base_url: string, allowedRoles: string[]) {
    return function (
        target: Object,
        propertyName: string,
        propertyDesciptor: PropertyDescriptor): PropertyDescriptor {
        // target === Employee.prototype
        // propertyName === "greet"
        // propertyDesciptor === Object.getOwnPropertyDescriptor(Employee.prototype, "greet")
        const method = propertyDesciptor.value;

        propertyDesciptor.value = async function (...args: any[]) {
            assert.ok(args && args.length >= 2, 'AnRoleAccessService: Invalid number of arguments for the request handler');

            let req = args[1];


            var token = extract_token_from_request(req);
            req.locals = req.locals || {};
            req.locals.token = token;
            delete req.query.token;

            const mf = new ModelFactory({ api_base_url: base_url, token: token })

            assert.ok(token, 'AnRoleAccessService: Failed to authenticate token.');

            let user = (req.locals && req.locals.user && req.locals.user.roles) ? req.locals.user : (await mf.anyModel(MODEL_PATHS.users).get('me'));
            let userRoles: string[] = user.roles || [];
            let roleIntersection = intersection(allowedRoles, userRoles);
            assert.ok(roleIntersection > 0, 'AnRoleAccessService: Invalid Role.Not Allowed.')

            const result = method.apply(this, args);

            return result;
        }
        return propertyDesciptor;
    };
}

export let AnRoleAccessService = gql_role_access;